from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.index, name='index'),
    path('index/',views.index, name='index'),
    path('profile/',views.profile, name='profile'),
    path('add_status/',views.add_status, name='add_status'),
]