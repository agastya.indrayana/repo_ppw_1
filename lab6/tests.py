from django.test import TestCase, Client
from django.urls import resolve
from . import views, models
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Lab6Test(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_lab_6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'lab6/homepage.html')

    def test_lab_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_lab_6_content_exist(self):
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Wellcome to my new website', html_response)  
        
    def test_model_can_create_new_status(self):
        #Creating a new activity
        new_activity = models.Status.objects.create(dateTime=timezone.now(),status='this is a test status')

        #Retrieving all available activity
        counting_all_available_activity = models.Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_can_save_a_POST_request_and_render(self):
        response = self.client.post('/add_status/', data={'status' : 'this is another test status'})
        counting_all_available_activity = models.Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('this is another test status', html_response)

    def test_lab_6_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_lab_6_profile_using_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'lab6/profile.html')

    def test_lab_6_profile_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, views.profile) 

    def test_lab_6_profile_content_exist(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertNotEqual(html_response,0)

class lab8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(lab8FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(lab8FunctionalTest, self).tearDown()

    def test_toggle_theme_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')

        body = selenium.find_element_by_tag_name('body')

        self.assertIn("rgba(255, 255, 255, 1)", body.value_of_css_property("background-color"))

        toggleButton = selenium.find_element_by_id('toggleButton')
        toggleButton.send_keys(Keys.RETURN)

        self.assertIn("rgba(17, 17, 17, 1)", body.value_of_css_property("background-color"))

    def test_homepage_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        assert "Homepage" in selenium.title

    def test_homepage_has_navbar(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        navbar = selenium.find_elements_by_tag_name("nav")
        assert len(navbar) > 0

    def test_form_uses_css(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        status_field = selenium.find_element_by_id('id_status')

        assert "form-control" in status_field.get_attribute("class")

    def test_navbar_uses_css(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        navbar = selenium.find_element_by_tag_name("nav")

        assert "navbar-inverse" in navbar.get_attribute("class")
    
    # def test_theme_button(self):
        
