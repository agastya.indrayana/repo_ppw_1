from django.shortcuts import render, redirect
from django.http import HttpResponse
from lab6.models import Status
from lab6.forms import statusForm
from datetime import datetime
import pytz


def index(request):
    statuses = Status.objects.all().values().order_by('dateTime')
    form = statusForm()
    context = {'statuss':statuses,'StatusForm':form} 
    return render(request, 'lab6/homepage.html', context)

def add_status(request):
    form = statusForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        dateTime = datetime.now()
        Status.objects.create(dateTime = dateTime.replace(tzinfo=pytz.UTC), status=request.POST['status'])
        return redirect ('/')

def profile(request):
    return render(request, "lab6/profile.html")